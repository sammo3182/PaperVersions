load("E:/Dropbox/Instruction/PhD/301 Interm Method/Final Paper/cgss2010II.rdata")
names(cgss2010)
cgss2010.use<-subset(cgss2010, select=c("panel","weight","region","region.1","nation.emotion","nation.offense","nation.emotion.1","nation.offense.1",              
                                        "generation.0","generation","gender","religion.0","religion","education","education.1","job","job.1", "urban","income.fam","income.sat",
                                        "sociolevel","manager.tem","manager","autonomy",
                                        "heal.phy.temp","heal.psy","tourism", "heal","pension","s.wealth",
                                        "happiness","environment",
                                        "ethnicity","ethn.xx","ethn.nhan","ethn.han","ethn.min",
                                        "han.listen","han.speak","han.comp","eng.listen","eng.speak","eng.comp",
                                        "newspaper","magzine","radio","tv","internet",
                                        "ccp","ccp.1","com.committee","poli.vote","union.committee",
                                        "poli.know.per","poli.part.self","poli.part.noinfl","poli.part.nocare","poli.part.deficit","poli.attention","poli.part.express","poli.know.obj","poli.part.talk","poli.protest",
                                        "trust.judicial","trust.central","trust.local","trust.military","trust.police","trust.media.central", "trust.legislature","trust.education","trust.media.local","trust.religion","trust.inter","trust.difference",
                                        "equality","conflict.poor","conflict.power","critical","baby.no.","location","individualism",
                                        "religion.obj","religion.strong","religion.science",
                                        "gov.eco","gov.service","gov.obey","poli.unjust","poli.respon","order","participation","economy","sp.free"))

#package preloading
ipak <- function(pkg){
  new.pkg <- pkg[!(pkg %in% installed.packages()[, "Package"])]
  if (length(new.pkg)) 
    install.packages(new.pkg, dependencies = TRUE)
  sapply(pkg, require, character.only = TRUE)
}

packages <- c("arm", "car","Amelia", "Zelig", "stargazer","gplots","texreg","lme4","lmerTest",
              "SASmixed","ggplot2","xtable", "doBy")
ipak(packages)

range01<-function(x)((x-min(na.omit(x))) / (max(na.omit(x))-min(na.omit(x)))) 

####loading the interplot function###
interplot <- function(m, var1, var2, seed=324, sims=1000, steps=100, plot=TRUE) {
  require(arm)
  require(ggplot2)
  set.seed(seed)
  m.sims <- sim(m, sims)
  
  var12 <- paste0(var2,":",var1)
  if (!var12 %in% names(m$coef)) var12 <- paste0(var1,":",var2)
  if (!var12 %in% names(m$coef)) stop(paste("Model does not include the interaction of",var1 ,"and",var2))
  coef <- data.frame(fake = seq(min(m$model[var2], na.rm=T), max(m$model[var2], na.rm=T), length.out=steps), coef1 = NA, ub = NA, lb = NA)
  
  for(i in 1:steps) {   
    coef$coef1[i] <- mean(m.sims@coef[,match(var1, names(m$coef))] + 
                            coef$fake[i]*m.sims@coef[,match(var12, names(m$coef))])
    coef$ub[i] <- quantile(m.sims@coef[,match(var1, names(m$coef))] + 
                             coef$fake[i]*m.sims@coef[,match(var12, names(m$coef))], .975)
    coef$lb[i] <- quantile(m.sims@coef[,match(var1, names(m$coef))] + 
                             coef$fake[i]*m.sims@coef[,match(var12, names(m$coef))], .025)    
  }
  
  if(plot==TRUE) {
    if(steps>5) {
      coef.plot <- ggplot(coef, aes(x = fake, y = coef1)) +                       
        geom_line() + geom_ribbon(aes(ymin=lb, ymax=ub), alpha=.5) 
    } else {
      coef.plot <- ggplot(coef, aes(x = fake, y = coef1)) +                       
        geom_point() + geom_errorbar(aes(ymin=lb, ymax=ub), width=0) + 
        scale_x_continuous(breaks = 0:steps)
    }
    return(coef.plot)
  } else {
    names(coef) <- c(var2, "coef", "ub", "lb")
    return(coef)
  }
}

#####Descriptive analysis of ethnic data各民族汉语能力百分比###############
cgss2010.des<-subset(cgss2010, panel==2010, select=c("region","generation","gender","religion","ethn.min","education.1",
                                                     "urban","income.sat","s.wealth","ccp.1",
                                                     "han.comp","eng.comp",
                                                     "radio","tv","internet","poli.part.talk",
                                                     "equality","critical","baby.no.","location", "individualism",
                                                     "trust.judicial","trust.central","trust.local","trust.military",
                                                     "trust.police","trust.media.central", "trust.legislature"))
stargazer(cgss2010.des,
          title="Effects of Mandarin Proficiency on the Cultural and Informative Political Influences", 
          covariate.labels=c("Generation","Gender","Religion","Ethnic Minority",
                             "Education","Urban",
                             "Satisfaction of Income","Social Welfare", "CCP Member",
                             "Mandarin Proficiency","English Proficiency",
                             "Radio", "TV", "Internet","Gossip",
                             "Democracy-Fairness","Democracy-Speech","Democracy-Birth Control","Democracy-Occupation",
                             "Democracy-Individualism",
                             "Trust-Judicatory","Trust-Central","Trust-Local","Trust-Military",
                             "Trust-Public Security", "Trust-Central Media","Trust-Legislature"),
          align=TRUE,
          notes="Source: 2010 CGSS.",
          style="ajps",     #automatically produce AJPS tables
          label="T:Description")

###Multiple imputation: main purpose is to estimate the nationalism####
cgss2010.mi<-subset(cgss2010.use, select=c("panel","weight","region","region.1",
                                           "nation.emotion.1","nation.offense.1",               
                                           "generation", "generation.0","gender","religion.0","religion", "ccp","ccp.1",
                                           "education","education.1", "urban","income.sat",
                                           "s.wealth",
                                           "ethnicity","ethn.min",
                                           "han.comp","eng.comp",
                                           "radio","tv","internet","poli.part.talk",
                                           "trust.judicial","trust.central","trust.local","trust.military","trust.police","trust.media.central", "trust.legislature"))
lapply(cgss2010.mi,summary)
table(cgss2010.mi$nation.emotion.1)

a.out<-amelia(cgss2010.mi,m=10, cs="region", ts="panel",
              ords=c("generation.0","religion.0","education","ethnicity",
                     "nation.emotion.1","nation.offense.1", "ccp"),
              norm=c("urban", "gender",),  
              idvars=c("weight", "region.1",        
                       "income.sat","s.wealth",
                       "ethn.min", "religion",
                       "han.comp","eng.comp",
                       "radio","tv","internet",
                       "poli.part.talk",
                       "trust.judicial","trust.central","trust.local","trust.military","trust.police","trust.media.central", "trust.legislature",
                       "generation", "education.1", "ccp.1"),
              p2s=0)

a.out.more<-amelia(cgss2010.mi,m=20, cs="region", ts="panel",
                   ords=c("generation.0","religion.0","education","ethnicity",
                          "nation.emotion.1","nation.offense.1", "ccp"),
                   norm=c("urban", "gender",),  
                   idvars=c("weight", "region.1",        
                            "income.sat","s.wealth",
                            "ethn.min", "religion",
                            "han.comp","eng.comp",
                            "radio","tv","internet",
                            "poli.part.talk",
                            "trust.judicial","trust.central","trust.local","trust.military","trust.police","trust.media.central", "trust.legislature",
                            "generation", "education.1", "ccp.1"),
                   p2s=0)
a.out.more <- ameliabind(a.out, a.out.more)
save(a.out, file = "cgss2010.mi.rdata")

#####test the influence of independent variables on nationalism###
s_0<-zelig(nation.emotion.1~han.comp+ethn.min
           +generation+religion+education.1+urban+income.sat+s.wealth+ccp.1+gender,
           weights=a.out.more$imputations$weight, data=a.out.more$imputations,
           model="ls")
summary(s_0)


s_01<-zelig(nation.emotion.1~han.comp,
            weights=a.out.more$imputations$weight, data=a.out.more$imputations,
            model="ls")
summary(s_01)

s_02<-zelig(nation.emotion.1~han.comp
            +ethn.min+generation+gender+religion,
            weights=a.out.more$imputations$weight, data=a.out.more$imputations,
            model="ls")
summary(s_02)

s_03<-zelig(nation.emotion.1~han.comp
            +ethn.min+generation+gender+religion
            +education.1+urban+income.sat,
            weights=a.out.more$imputations$weight, data=a.out.more$imputations,
            model="ls")
summary(s_03)

s_04<-zelig(nation.emotion.1~han.comp
            +ethn.min+generation+gender+religion
            +education.1+urban+income.sat
            +s.wealth+ccp.1,
            weights=a.out.more$imputations$weight, data=a.out.more$imputations,
            model="ls")
summary(s_04)

s_05<-zelig(nation.emotion.1~han.comp
            +ethn.min+generation+gender+religion
            +urban+income.sat
            +s.wealth+ccp.1,
            weights=a.out.more$imputations$weight, data=a.out.more$imputations,
            model="ls")
summary(s_05)

s_06<-zelig(nation.emotion.1~han.comp
            +ethn.min+generation+gender+religion
            +education.1+urban+income.sat
            +s.wealth+ccp.1
            +generation:education.1,
            weights=a.out.more$imputations$weight, data=a.out.more$imputations,
            model="ls")
summary(s_06)

#Trust and Language###

#0.Isolate the dataset and set the index####
cgss2010.use.all<-na.omit(subset(cgss2010.use, select=c("weight","region","region.1","generation","gender","religion","religion.0","education.1","urban","income.sat",                                                        
                                                        "sociolevel","s.wealth",
                                                        "ethnicity","ethn.xx","ethn.nhan","ethn.han","ethn.min",
                                                        "han.comp","eng.comp",
                                                        "radio","tv","internet",
                                                        "ccp.1","poli.part.talk",
                                                        "equality","critical","baby.no.","location",
                                                        "trust.education","trust.religion","trust.inter",
                                                        "trust.judicial","trust.central","trust.local","trust.military","trust.police","trust.media.central", 
                                                        "trust.legislature")))

cgss2010.trust.all<-subset(cgss2010.use.all, select=c(trust.judicial:trust.legislature))

cgss2010.trust.pca<-princomp(formula = ~., data = cgss2010.trust.all, cor = TRUE, na.action=na.omit)
plot(cgss2010.trust.pca)  #几个柱高，基本上就是几个factor

trust.index<-factanal(cgss2010.trust.all, factor=1,
                      score="Bartlett")
cgss2010.use.all$trust.index<-range01(trust.index$scores)
summary(cgss2010.use.all$trust.index)
table(cgss2010.use.all$trust.index) #Showing that this is a continuous variable

#1. basic socioeconomic and social political models#####
s_7.0.3<-lm(trust.index~generation+gender+religion+ethn.min
            +education.1+urban+income.sat+s.wealth+ccp.1
            +radio+tv+internet+poli.part.talk,
            data=cgss2010.use.all, weights=weight) 
summary(s_7.0.3)

#2. Cultural Function####
s_7.0.5<-lm(trust.index~generation+gender+religion+ethn.min
            +education.1+urban+income.sat+s.wealth+ccp.1
            +han.comp+eng.comp
            +ethn.min:han.comp,
            data=cgss2010.use.all) 
summary(s_7.0.5)

#3. Media Function####
s_7.1.3<-lm(trust.index~generation+gender+religion+ethn.min
            +education.1+urban+income.sat+s.wealth+ccp.1
            +radio+tv+internet+poli.part.talk
            +han.comp+eng.comp
            +radio:han.comp+tv:han.comp+internet:han.comp+poli.part.talk:han.comp,
            data=cgss2010.use.all) 
summary(s_7.1.3) #加入cultural interaction后R只增长.008，所以为了避免junk regression，两个分看run

s_7.1.4<-lm(trust.index~generation+gender+religion+ethn.min
            +education.1+urban+income.sat+s.wealth+ccp.1
            +radio+tv+internet+poli.part.talk
            +han.comp+eng.comp
            +ethn.min:han.comp
            +radio:han.comp+tv:han.comp+internet:han.comp+poli.part.talk:han.comp,
            data=cgss2010.use.all) 
summary(s_7.1.4)

#4. Visualized the plot####
#detach a bunch of package,
detachAllPackages <- function() {
  
  basic.packages <- c("package:stats","package:graphics","package:grDevices","package:utils","package:datasets","package:methods","package:base")
  
  package.list <- search()[ifelse(unlist(gregexpr("package:",search()))==1,TRUE,FALSE)]
  
  package.list <- setdiff(package.list,basic.packages)
  
  if (length(package.list)>0)  for (package in package.list) detach(package, character.only=TRUE)
  
}

detachAllPackages()
#reload some packages
needed.packages <- c("foreign", "stargazer", "ggplot2", "rstan","arm", "car")
lapply(needed.packages, require, character.only=T)

p1 <- interplot(m=s_7.1.4, var1="ethn.min", var2="han.comp")
p1 <- p1 + ylab("Coefficient for Ethnicity") + xlab("Mandarin Proficiency")+ theme_bw()
png(file = "E:/Dropbox/Instruction/PhD/301 Interm Method/Final Paper/InterplotEM.png",
    width = 852, height = 480)
p4
dev.off()
# 汉语力量将民族差异弄没了


p4 <- interplot(m=s_7.1.4, var1="tv", var2="han.comp")
p4 <- p4 + ylab("Coefficient for TV") + xlab("Mandarin Proficiency")+ theme_bw()
png(file = "E:/Dropbox/Instruction/PhD/301 Interm Method/Final Paper/InterplotTM.png",
    width = 852, height = 480)
p4
dev.off()

p5 <- interplot(m=s_7.1.4, var1="internet", var2="han.comp")
p5 <- p5 + ylab("Coefficient for Internet") + xlab("Mandarin Proficiency")+ theme_bw()
png(file = "E:/Dropbox/Instruction/PhD/301 Interm Method/Final Paper/InterplotIM.png",
    width = 852, height = 480)
p5
dev.off()

#5. Multilevel Model####
###Visualize Ethnicity Difference###
cgss2010.use.all$ethnicity<-factor(cgss2010.use.all$ethnicity,  
                                   levels=c(1:8),
                                   labels=c("Han","Mongolian","Manchu","Hui","Tibetan","Zhuang","Uyghur","Others"))
table(cgss2010.use.all$ethnicity)


#reload the packages first
png(file = "E:/Dropbox/Instruction/PhD/301 Interm Method/Final Paper/HeterTrustT.png",
    width = 852, height = 480)
plotmeans(trust.index~ethnicity, n.label=F,
          bars=T,
          xlab="Ethnicity", 
          ylab="Political Trust",
          data= cgss2010.use.all)  #存在明显差异，但满族人最低，维吾尔人最高。
dev.off()

###Multilevel with lme4->结果是不需要multilevel， random effect是0###
s_7.01.c<-lmer(trust.index~han.comp+eng.comp
               +generation+gender+religion+education.1
               +urban+income.sat
               +s.wealth+ccp.1
               +radio+tv+internet+poli.part.talk
               +(1|ethnicity),
               data=cgss2010.use.all, weights=weight)  #vary intercept

####vary intercept+slope(only language variable), result unsig###
s_7.01.cs<-lmer(trust.index~han.comp+eng.comp
                +generation+gender+religion+education.1
                +urban+income.sat
                +s.wealth+ccp.1
                +radio+tv+internet+poli.part.talk
                +(1+han.comp|ethnicity),
                data=cgss2010.use.all, weights=weight)

LRTc=anova(s_7.01.c, s_7.01.cs) 
LRTc  #insign, choose 7.01.c.


#Democratic values and Language####
#0. Isolate the dataset and Democratic Value Index####
cgss2010.use.all2<-na.omit(subset(cgss2010.use, select=c("weight","region","region.1","generation","gender","religion","religion.0","education.1","urban","income.sat",                                                        
                                                         "s.wealth","ccp.1",
                                                         "ethnicity","ethn.xx","ethn.nhan","ethn.han","ethn.min",
                                                         "han.comp","eng.comp",
                                                         "radio","tv","internet","poli.part.talk",
                                                         "equality","critical","baby.no.","location","individualism")))

cgss2010.demo.all<-subset(cgss2010.use.all2, select=c(equality:individualism))

cgss2010.demo.pca<-princomp(formula = ~., data = cgss2010.demo.all, cor = TRUE, na.action=na.omit)
plot(cgss2010.demo.pca)  #几个柱高，基本上就是几个factor

demo.index<-factanal(cgss2010.demo.all, factor=1,
                     score="Bartlett")
cgss2010.use.all2$demo.index<-range01(demo.index$scores)  #index

#1. basic socioeconomic and social political models####
s_7.2.3<-lm(demo.index~generation+gender+religion+ethn.min
            +education.1+urban+income.sat+s.wealth+ccp.1
            +radio+tv+internet+poli.part.talk,
            data=cgss2010.use.all2, weights=weight) 
summary(s_7.2.3)

#2. Cultural Function####
s_7.2.5<-lm(demo.index~generation+gender+religion+ethn.min
            +education.1+urban+income.sat+s.wealth+ccp.1
            +han.comp+eng.comp
            +ethn.min:han.comp,
            data=cgss2010.use.all2) 
summary(s_7.2.5)

#3. Media Function####
s_7.3.3<-lm(demo.index~generation+gender+religion+ethn.min
            +education.1+urban+income.sat+s.wealth+ccp.1
            +radio+tv+internet+poli.part.talk
            +han.comp+eng.comp
            +radio:han.comp+tv:han.comp+internet:han.comp+poli.part.talk:han.comp,
            data=cgss2010.use.all2) 
summary(s_7.3.3) #加入cultural interaction后R只增长.008，所以为了避免junk regression，两个分看run

s_7.3.4<-lm(demo.index~generation+gender+religion+ethn.min
            +education.1+urban+income.sat+s.wealth+ccp.1
            +radio+tv+internet+poli.part.talk
            +han.comp+eng.comp
            +ethn.min:han.comp
            +radio:han.comp+tv:han.comp+internet:han.comp+poli.part.talk:han.comp,
            data=cgss2010.use.all2) 
summary(s_7.3.4)
#Visualize Interactions###
p7 <- interplot(m=s_7.3.4, var1="ethn.min", var2="han.comp")
p7 <- p7 + ylab("Coefficient for Ethnicity") + xlab("Mandarin Proficiency")
png(file = "E:/Dropbox/Instruction/PhD/301 Interm Method/Final Paper/InterplotEMD.png",
    width = 852, height = 480)
p7+ theme_bw()  # 汉语力量将民族差异于民主消弭了
dev.off()

p8 <- interplot(m=s_7.3.4, var1="radio", var2="han.comp")
p8 <- p8 + ylab("Coefficient for Radio") + xlab("Mandarin Proficiency")
png(file = "E:/Dropbox/Instruction/PhD/301 Interm Method/Final Paper/InterplotRMD.png",
    width = 852, height = 480)
p8+ theme_bw()
dev.off()

p9 <- interplot(m=s_7.3.4, var1="tv", var2="han.comp")
p9 <- p9 + ylab("Coefficient for TV") + xlab("Mandarin Proficiency")
png(file = "E:/Dropbox/Instruction/PhD/301 Interm Method/Final Paper/InterplotTMD.png",
    width = 852, height = 480)
p9+ theme_bw()
dev.off()

stargazer(s_7.0.3, s_7.0.5, s_7.1.3, s_7.1.4, 
          title="Impacts of Mandarin Proficiency with Regards to Political Trust", 
          dep.var.labels="Political Trust",
          column.labels=c("Basic Model","Cultural Function Model","Informative Function Model","Dual Function Model"),
          model.numbers=F,  #When it's TRUE, the table shows what kind of regression "OLS", "probit".etc
          covariate.labels=c("Constant","Generation","Gender","Religion","Ethnic minority",
                             "Education","Urban",
                             "Satisfaction of income","Social wealfare", "CCP",
                             "Radio", "TV", "Internet","Gossip",
                             "Mandarin Proficiency","English Proficiency",
                             "Ethnicity$times$Mandarin Proficiency",
                             "Radio$times$Mandarin Proficiency","TV$times$Mandarin Proficiency",
                             "Internet$times$Mandarin Proficiency","Gossip$times$Mandarin Proficiency"),
          align=TRUE,       #coefficients in each column are aligned along the decimal point.
          digits=3,
          intercept.bottom = F, intercept.top = T,
          font.size="tiny",
          notes="Source: 2010 CGSS.",
          style="ajps",     #automatically produce AJPS tables
          label="T:InteractionT")

stargazer(s_7.2.3, s_7.2.5, s_7.3.3, s_7.3.4,
          title="impacts of Mandarin Proficiency with Regards to Democratic Propensity", 
          dep.var.labels="Democratic Value Index",
          column.labels=c("Basic Model","Cultural Function Model","Informative Function Model","Dual Function Model"),
          model.numbers=F,  #When it's TRUE, the table shows what kind of regression "OLS", "probit".etc
          covariate.labels=c("Constant","Generation","Gender","Religion","Ethnic minority",
                             "Education","Urban",
                             "Satisfaction of income","Social wealfare", "CCP",
                             "Radio", "TV", "Internet","Gossip",
                             "Mandarin Proficiency","English Proficiency",
                             "Ethnicity$times$Mandarin Proficiency",
                             "Radio$times$Mandarin Proficiency","TV$times$Mandarin Proficiency",
                             "Internet$times$Mandarin Proficiency","Gossip$times$Mandarin Proficiency"),
          align=TRUE,       #coefficients in each column are aligned along the decimal point.
          digits=3,
          intercept.bottom = F, intercept.top = T,
          font.size="tiny",
          notes="Source: 2010 CGSS.",
          style="ajps",     #automatically produce AJPS tables
          label="T:InteractionD")
#3.1 Dig in more about radio####
factor<-c("generation","gender","religion","ethn.min",
          "education.1","urban","income.sat","s.wealth","ccp.1",
          "tv","internet","poli.part.talk")

cgss2010.use.all2$ethnicity<-factor(cgss2010.use.all2$ethnicity,  
                                   levels=c(1:8),
                                   labels=c("Han","Mongolian","Manchu","Hui","Tibetan","Zhuang","Uyghur","Others"))
table(cgss2010.use.all2$ethnicity)



png(file = "E:/Dropbox/Instruction/PhD/301 Interm Method/Final Paper/HeterRE.png",
    width = 852, height = 480)
plotmeans(radio~ethnicity,
          n.label=F,
          bars=T,ylab="Radio Frequency",
          data= cgss2010.use.all2)
dev.off()



#only CI of generation, religion, ethn.min, income.sat overlap. 
# 以语言为基础的民族分类
cgss2010.use.all2$ethn.nhan<-ifelse(cgss2010.use.all2$ethnicity%in%c(1,3,4), 0,1)
table(cgss2010.use.all2$ethn.nhan)


s_7.4.1<-lm(demo.index~generation+gender+religion+ethn.min
            +education.1+urban+income.sat+s.wealth+ccp.1
            +radio+tv+internet+poli.part.talk
            +han.comp+eng.comp
            +ethn.min:han.comp
            +radio:han.comp+tv:han.comp+internet:han.comp+poli.part.talk:han.comp
            +radio:ethn.min,
            data=cgss2010.use.all2) 
summary(s_7.4.1)

stargazer(s_7.4.1, 
          title="An Explanation for the Effect of Radio", 
          dep.var.labels="Political Trust",
          column.labels=c("Basic Model","Cultural Function Model","Informative Function Model","Dual Function Model"),
          model.numbers=F,  #When it's TRUE, the table shows what kind of regression "OLS", "probit".etc
          covariate.labels=c("Constant","Generation","Gender","Religion","Ethnic minority",
                             "Education","Urban",
                             "Satisfaction of income","Social wealfare", "CCP",
                             "Radio", "TV", "Internet","Gossip",
                             "Mandarin Proficiency","English Proficiency",
                             "Ethnicity$times$Mandarin Proficiency",
                             "Radio$times$Mandarin Proficiency","TV$times$Mandarin Proficiency",
                             "Internet$times$Mandarin Proficiency","Gossip$times$Mandarin Proficiency",
                             "Radio$times$Ethnic Minority"),
          align=TRUE,       #coefficients in each column are aligned along the decimal point.
          digits=3,
          font.size="scriptsize",
          intercept.bottom = F, intercept.top = T,
          notes="Source: 2010 CGSS.",
          style="ajps",     #automatically produce AJPS tables
          label="T:InteractionR")

#detach a bunch of package,
detachAllPackages <- function() {
  
  basic.packages <- c("package:stats","package:graphics","package:grDevices","package:utils","package:datasets","package:methods","package:base")
  
  package.list <- search()[ifelse(unlist(gregexpr("package:",search()))==1,TRUE,FALSE)]
  
  package.list <- setdiff(package.list,basic.packages)
  
  if (length(package.list)>0)  for (package in package.list) detach(package, character.only=TRUE)
  
}

detachAllPackages()
#reload some packages
needed.packages <- c("foreign", "stargazer", "ggplot2", "rstan","arm", "car")
lapply(needed.packages, require, character.only=T)

p10 <- interplot(m=s_7.4.1, var1="radio", var2="ethn.min")
p10 <- p10 + ylab("Coefficient for Radio") + xlab("Ethnic minority")
png(file = "E:/Dropbox/Instruction/PhD/301 Interm Method/Final Paper/InterplotRED.png",
    width = 852, height = 480)
p10+ theme_bw()
dev.off()

p11 <- interplot(m=s_7.4.1, var1="radio", var2="han.comp")
p11 <- p11 + ylab("Coefficient for Radio") + xlab("Mandarin Proficiency")
png(file = "E:/Dropbox/Instruction/PhD/301 Interm Method/Final Paper/InterplotRMD2.png",
    width = 852, height = 480)
p11+ theme_bw()
dev.off()

#4. Multilevel Model####
###Visualize Ethnicity Difference###
#Reload the packages
png(file = "E:/Dropbox/Instruction/PhD/301 Interm Method/Final Paper/HeterDemo.png",
    width = 852, height = 480)
plotmeans(demo.index~ethnicity, n.label=F,
          bars=T,
          xlab="Ethnicity", 
          ylab="Democracy Propensity",
          data= cgss2010.use.all2)  #存在明显差异，但满族人最低，维吾尔人最高。
dev.off()

###Multilevel with lme4->结果是不需要multilevel， random effect是0###
s_7.21.c<-lmer(demo.index~han.comp+eng.comp
               +generation+gender+religion+education.1
               +urban+income.sat
               +s.wealth+ccp.1
               +radio+tv+internet+poli.part.talk
               +(1|ethnicity),
               data=cgss2010.use.all2, weights=weight)  #vary intercept


####vary intercept+slope(only language variable), result unsig###
s_7.21.cs<-lmer(demo.index~han.comp+eng.comp
                +generation+gender+religion+education.1
                +urban+income.sat
                +s.wealth+ccp.1
                +radio+tv+internet+poli.part.talk
                +(1+han.comp|ethnicity),
                data=cgss2010.use.all2, weights=weight)


LRTcD=anova(s_7.21.c, s_7.21.cs) #LRS on 2 level or 3 level
LRTcD

print(xtable(LRTcD), digits=3,latex.environments=("center"))

stargazer(LRTc, LRTcD,
          title=c("Likelihood Ratio Test for the Slop Variance Regarding Ethnicity"),
          align=TRUE,       #coefficients in each column are aligned along the decimal point.
          digits=3,
          notes="Source: 2010 CGSS.",
          omit.summary.stat=c("n","mean", "sd"),
          style="ajps",     #automatically produce AJPS tables
          label=c("T:LRTc","T:LRTcD"))

texreg(list(s_7.01.c),use.packages=FALSE, 
       dcolumn = TRUE, booktabs = TRUE,
       label="T:ml", caption="Multilevel Effect on Mandarin Proficiency",
       custom.model.names=c("Trust-Ethnicity"),
       custom.coef.names=c("Constant", "Mandarin Proficiency","English Proficiency","Generation","Gender",
                           "Education","Urban","Satisifaction of Income","Social Welfare",
                           "CCP",
                           "Radio","TV","Internet","Gossip"),
       scriptsize=T,float.pos="htbp", digits=3)

